
use std::thread;
use std::time::Duration;

//fn simulated_calculation(intensity: u32) -> u32 {
//    println!("calculating slowly..");
//    thread::sleep(Duration::from_secs(2));
//    intensity
//}

pub struct Cacher<T>
    where T: Fn(u32) -> u32
{
    calculation: T,
    value: Option<u32>,
}

impl<T> Cacher<T>
    where T: Fn(u32) -> u32
{
    fn new(calculation: T) -> Cacher<T> {
        Cacher {
            calculation,
            value: None,
        }
    }

    fn value(&mut self, arg: u32) -> u32 {
        match self.value {
            Some(v) => v,
            None => {
                let v = (self.calculation)(arg);
                self.value = Some(v);
                v
            },
        }
    }
}

fn generate_workout(intensity: u32, rand_num: u32) {
    let mut simlated_closure = Cacher::new(|num| {
        println!("calculating slowly..");
        thread::sleep(Duration::from_secs(2));
        num
    });

    if intensity < 25 {
        println!(
            "Today, do {} pushups!",
            simlated_closure.value(intensity)
        );
        println!(
            "Next, do {} situps!",
            simlated_closure.value(intensity)
        );
    } else {
        if rand_num == 3 {
            println!("Take a break today! Drink electrolytes!");
        } else {
            println!(
                "Today, run for {} minutes!",
                simlated_closure.value(intensity)
            );
        }
    }
}

fn main() {
//    //capturing env with closures
//    let x = 4;
//
//    let equal_to = |z| z== x;
//
//    let y = 4;
//
//    assert!(equal_to(y));

    //using move
    let x = vec![2, 5, 7];
    let equal_to = move |z| z == x;
    //println!("cant use x here: {:?}", x);
    let y = vec![2, 5, 7];

    assert!(equal_to(y));


    //cache struct example
    let simulated_user_val = 10;
    let simulated_rnd_num = 7;

    generate_workout(
        simulated_user_val,
        simulated_rnd_nu
    );

}


//#[cfg(test)]
//mod tests {
//    use super::*;
//    #[test]
//    fn call_with_different_values() {
//        let mut c = Cacher::new(|a| a);
//
//        let v1 = c.value(1);
//        let v2 = c.value(2);
//
//        assert_eq!(v2, 2);
//    }
//}