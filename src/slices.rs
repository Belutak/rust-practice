
#![allow(unused_variables)]
fn main() {
    let mut s = String::from("hello world");


    //test gitpush on new machine
    let word = first_word(&s); // word will get the value 5

    dbg!(word);
    s.clear();

//    let hello = &s[0..5];
//   let world = &s[6..11];

//    let my_string = String::from("hello world");
//
//    // first_word works on slices of `String`s
//    let word = first_word(&my_string[..]);
//
//    let my_string_literal = "hello world";
//
//    // first_word works on slices of string literals
//    let word = first_word(&my_string_literal[..]);
//
//    // Because string literals *are* string slices already,
//    // this works too, without the slice syntax!
//    let word = first_word(my_string_literal);

}

//fn first_word(s: &String) -> usize {
//    let bytes = s.as_bytes();
//
//    for (i, &item) in bytes.iter().enumerate() {
//        if item == b' ' {
//            return i;
//        }
//
//    }
//
//    s.len()
//}

fn first_word(s: &String) -> &str {
    let bytes = s.as_bytes();

    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return &s[0..i];
        }
    }
    &s[..]
}
