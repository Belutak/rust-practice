

struct Context<'s>(&'s str);

struct Parser<'c, 's: 'c> {
    context: &'c Context<'s>,
}

impl<'c, 's> Parser<'c, 's> {
    fn parse(&self) -> Result<(), &'s str> {
        Err(&self.context.0[1..])
    }
}

fn parse_context(context: Context) -> Result<(), &str> {
    Parser { context: &context }.parse()
}


struct StaticRef<T: 'static>(&'static T);
struct Ref<'a, T: 'a>(&'a T);


//inference of trait object lifetimes
trait Red { }

struct Ball<'a> {
    diameter: &'a i32,
}

//The anonymous lifetime
struct StrWrap<'a>(&'a str);

//fn foo<'a>(string: &'a str) -> StrWrap<'a> {
//    StrWrap(string)
//}
fn foo(string: &str) -> StrWrap<'_> {
    StrWrap(string)
}

impl<'a> Red for Ball<'a> { }

fn main() {
    //inference of trait object lifetimes
    let num = 5;

    let obj = Box::new(Ball { diameter: &num }) as Box<dyn Red>;



}
