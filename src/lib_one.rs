

//pub use kinds::PrimaryColor;
//pub use kinds::SecondaryColor;
//pub use utils::mix;
//
//pub mod kinds {
//    /// The primary colors according to the RYB color model.
//    pub enum PrimaryColor {
//        Red,
//        Yellow,
//        Blue,
//    }
//
//    /// The secondary colors according to the RYB color model.
//    pub enum SecondaryColor {
//        Orange,
//        Green,
//        Purple,
//    }
//}
//
//pub mod utils {
//    use crate::kinds::*;
//
//    /// Combines two primary colors in equal amounts to create
//    /// a secondary color.
//    pub fn mix(c1: PrimaryColor, c2: PrimaryColor) -> SecondaryColor {
//        // code logic
//
//    }
//}

//#![allow(dead_code)]
//#[derive(PartialEq, Debug)]
//struct Shoe {
//    size: u32,
//    style: String,
//}
//
//fn shoes_in_my_size(shoes: Vec<Shoe>, shoe_size: u32) -> Vec<Shoe> {
//    shoes.into_iter().filter(|s| s.size == shoe_size).collect()
//}

////! # My Crate
////!
////! `my_crate` is a collection of utilities to make performing certain
////! calculations more convenient.
//
///// Adds one to the number given.
/////
///// # Examples
/////
///// ```
///// let five = 5;
/////
///// assert_eq!(6, branches::add_one(5));
///// ```
//pub fn add_one(x: i32) -> i32 {
//    x + 1
//}
//

//mod main;

//#[derive(Debug)]
//pub struct Rectangle {
//    length: u32,
//    width: u32,
//}
//
//impl Rectangle {
//    pub fn can_hold(&self, other: &Rectangle) -> bool {
//        self.length > other.length && self.width > other.width
//    }
//}
//
////pub fn add_two(a: i32) -> i32 {
////    a + 2
////}
//
//pub fn greetings(name: &str) -> String {
//    format!("hello {}", name)
//}
//
//pub struct Guess {
//    value: i32,
//}
//
//impl Guess {
//    pub fn new(value: i32) -> Guess {
//        if value < 1 {
//            panic!("Guess val must be larger than 0. Found val is {}", value);
//        } else if value > 100 {
//            panic!("Guess val must be smaller than 100. Found val is {}", value);
//        }
//
//        Guess {
//            value
//        }
//    }
//}
//
//
//
//
//
//
//
//
//fn print_and_return_10(a: i32) -> i32 {
//    println!("I got the val {}", a);
//    10
//}
//
//
//
//pub fn add_two(a: i32) -> i32 {
//    internal_adder(a, 2)
//}
//
//fn internal_adder(a: i32, b: i32) -> i32 {
//    a + b
//}

//#[cfg(test)]
//mod tests {
//    use super::*;
//
//    #[test]
//    fn filters_by_size() {
//        let shoes = vec![
//            Shoe {
//                size: 10,
//                style: String::from("sneaker"),
//            },
//            Shoe {
//                size: 13,
//                style: String::from("sandal"),
//            },
//            Shoe {
//                size: 10,
//                style: String::from("boot"),
//            },
//        ];
//
//        let in_my_size = shoes_in_my_size(shoes, 10);
//
//        assert_eq!(
//            in_my_size,
//            vec![
//                Shoe {
//                    size: 10,
//                    style: String::from("sneaker")
//                },
//                Shoe {
//                    size: 10,
//                    style: String::from("boot")
//                },
//            ]
//        );
//    }
//
//    #[test]
//    fn iterator_demonstration() {
//        let v1 = vec![1, 2, 3];
//
//        let mut v1_iter = v1.iter();
//
//        assert_eq!(v1_iter.next(), Some(&1));
//        assert_eq!(v1_iter.next(), Some(&2));
//        assert_eq!(v1_iter.next(), Some(&3));
//        assert_eq!(v1_iter.next(), None);
//    }
//
//    #[test]
//    fn iterator_sum() {
//        let v1 = vec![1, 2, 3];
//
//        let v1_iter = v1.iter();
//
//        let total: i32 = v1_iter.sum();
//
//        assert_eq!(total, 6);
//    }
//    #[test]
//    fn internal() {
//        assert_eq!(4, internal_adder(2, 2));
//    }
//
//
//    #[test]
//    fn test_that_passes() {
//        let value = print_and_return_10(4);
//        assert_eq!(10, value);
//    }
//
////    #[test]
////    fn test_that_fails() {
////        let value = print_and_return_10(2);
////        assert_eq!(6, value);
////    }
//
//
//
//
//
//    #[test]
//    fn it_works() -> Result<(), String> {
//        if 2 + 2 == 4 {
//            Ok(())
//        } else {
//            Err(String::from("two plus two does not equal four"))
//        }
//    }
//
//    #[test]
//    #[should_panic(expected = "Guess val must be smaller than 100.")]
//    fn guss_too_high() {
//        Guess::new(200);
//    }
//
//    #[test]
//    fn greetings_name() {
//        let result = greetings("Carol");
//        assert!(
//            result.contains("Carol"), "Greeting did not \
//             have name, val was {}", result
//        );
//    }
//
//    #[test]
//    fn larger_can_hold_smaller() {
//        let larger = Rectangle { length: 8, width: 7 };
//        let smaller = Rectangle { length: 5, width: 1};
//
//        assert!(larger.can_hold(&smaller));
//    }
//
//    #[test]
//    fn smaller_cannot_hold_larger() {
//        let larger = Rectangle { length: 8, width: 7 };
//        let smaller = Rectangle { length: 5, width: 1 };
//
//        assert!(!smaller.can_hold(&larger));
//    }
//
//    #[test]
//    fn exploration() {
//        assert_eq!(2 + 2, 4);
//    }
////
////    #[test]
////    fn call_with_different_values() {
////        let mut c = Cacher::new(|a| a);
////
////        let v1 = c.value(1);
////        let v2 = c.value(2);
////
////        assert_eq!(v2, 2);
////    }
////    #[test]
////    fn another() {
////        panic!("Thus shall fail!");
////    }
//
////    #[test]
////    fn add_two_test() {
////        assert_eq!(4, add_two(2));
////    }
//
//
////#[test]
////fn call_with_different_values() {
////    let mut c = main::Cacher::new(|a| a);
////
////    let v1 = c.value(1);
////    let v2 = c.value(2);
////
////    assert_eq!(v2, 2);
////}
//}
