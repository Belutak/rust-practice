//use List::{Cons, Nil};
use std::rc::{Rc, Weak};
use std::cell::RefCell;

//#[derive(Debug)]
//enum List {
//    Cons(i32, RefCell<Rc<List>>),
//    Nil,
//}
//
//impl List {
//    fn tail(&self) -> Option<&RefCell<Rc<List>>> {
//        match self {
//            Cons(_, item) => Some(item),
//            Nil => None,
//        }
//
//    }
//}


#[derive(Debug)]
struct Node {
    value: i32,
    parent: RefCell<Weak<Node>>,
    children: RefCell<Vec<Rc<Node>>>,
}
fn main() {


    fn main() {
        let leaf = Rc::new(Node {
            value: 3,
            parent: RefCell::new(Weak::new()),
            children: RefCell::new(vec![]),
        });

        println!(
            "leaf strong = {}, weak = {}",
            Rc::strong_count(&leaf),
            Rc::weak_count(&leaf),
        );

        {
            let branch = Rc::new(Node {
                value: 5,
                parent: RefCell::new(Weak::new()),
                children: RefCell::new(vec![Rc::clone(&leaf)]),
            });

            *leaf.parent.borrow_mut() = Rc::downgrade(&branch);

            println!(
                "branch strong = {}, weak = {}",
                Rc::strong_count(&branch),
                Rc::weak_count(&branch),
            );

            println!(
                "leaf strong = {}, weak = {}",
                Rc::strong_count(&leaf),
                Rc::weak_count(&leaf),
            );
        }

        println!("leaf parent = {:?}", leaf.parent.borrow().upgrade());
        println!(
            "leaf strong = {}, weak = {}",
            Rc::strong_count(&leaf),
            Rc::weak_count(&leaf),
        );
    }

    //tree, Weak<T>
//    let leaf = Rc::new(Node {
//        value: 3,
//        parent: RefCell::new(Weak::new()),
//        children: RefCell::new(vec![]),
//    });
//
//    println!("leaf parent = {:?}", leaf.parent.borrow().upgrade());
//
//    let branch = Rc::new(Node {
//        value: 5,
//        parent: RefCell::new(Weak::new()),
//        children: RefCell::new(vec![Rc::clone(&leaf)]),
//    });
//
//    *leaf.parent.borrow_mut() = Rc::downgrade(&branch);
//
//    println!("leaf parent = {:?}", leaf.parent.borrow().upgrade());


    //cyrcular ref/memory leak
//    let a = Rc::new(Cons(5, RefCell::new(Rc::new(Nil))));
//
//    println!("a initial rc count is = {}", Rc::strong_count(&a));
//    println!("a = {:?}", &a);
//    println!("a next item = {:?}", a.tail());
//
//    let b = Rc::new(Cons(10, RefCell::new(Rc::clone(&a))));
//
//    println!("a rc count after b creation = {}", Rc::strong_count(&a));
//    println!("b initial rc count is = {}", Rc::strong_count(&b));
//    println!("b = {:?}", &b);
//    println!("b next item is = {:?}", b.tail());
//
//    if let Some(link) = a.tail() {
//        *link.borrow_mut() = Rc::clone(&b);
//    }
//
//    println!("b rc count after changing a = {}", Rc::strong_count(&b));
//    println!("a rc count after changing a = {}", Rc::strong_count(&a));
//
//    println!("a next item = {:?}", a.tail());
    //multiple pointers to mutable value
//    let value = Rc::new(RefCell::new(5));
//
//    let a = Rc::new(Cons(Rc::clone(&value), Rc::new(Nil)));
//
//    let b = Cons(Rc::new(RefCell::new(6)), Rc::clone(&a));
//    let c = Cons(Rc::new(RefCell::new(10)), Rc::clone(&a));
//
//    *value.borrow_mut() += 10;
//
//    println!("a after = {:?}", a);
//    println!("b after = {:?}", b);
//    println!("c after = {:?}", c);



}