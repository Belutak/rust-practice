
//fn five() -> i32 {
//    5
//}


fn main() {
//    println!("Hello, world!");
//
//    let tup = (500, 5.4, 2);
//    let (x, y, z) = tup;
//    println!("val y is {}", y);


//    let _x = 5;
//
//    let y = {
//        let x = 3;
//        x + 1
//    };
//
//    println!("The val of y is: {}", y);


//    let x = five();
//
//    println!("The val of x is: {}", x);


    let x = plus_one(5);

    println!("The val of x is: {}", x);
}

fn plus_one(x: i32) -> i32 {
    x + 1
}