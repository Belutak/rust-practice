//use IpAddrKind::V4;



fn main() {


 //   let x = IpAddrKind::V4;
//    println!("{:?}", x);

//    let some_address = IpAddr {
//        kind: IpAddrKind::V4,
//        address: String::from("first address")
//    };
//    let some_address = IpAddrKind::V4(String::from("123.3.2.213"));
//
//  //  println!("some addr {:?}", some_address.kind);
//
//    let sum_num = Some(5);
//
//
//    let sum_numm = V4(String::from("kk"));
//
//    dbg!(sum_numm);
    let money = Coin::Quarter(USState::California);
//    println!("coin is {:?}", money);
//
//    println!("call to f {:?}", coin_type(money));
//
//    println!("call to f2 {:?}", coin_type(Coin::Quarter(USState::Texas)));

    //let x: Option<i32> = Some(3);
    let x = Some(3);
    println!("val of x is {:?}", add_value(x));

    let y = 1;

    match y {
        5 => println!("cool"),
        4 => println!("less cool"),
        3 => println!("not cool"),
        _ => (),
    }

    let mut count = 0;
    if let Coin::Quarter(state) = money {
        println!("coin is {:?}", state);
    } else {
        count += 1;
    }


    let num = Some(3);

    if let Some(3) = num {
        println!("apex code");
    }

}

fn add_value(x: Option<i32>) -> Option<i32> {
    match x {
        None => None,
        Some(i) => Some(i + 1),
    }
}
#[derive(Debug)]
enum Coin {
    Penny,
    Nickel,
    Dime,
    Quarter(USState),
}
#[derive(Debug)]
enum USState {
    California,
    Florida,
    Texas,
}
//pub enum IpAddrKind {
//    V4(String),
//    V6(String),
//}

//#[derive(Debug)]
//struct IpAddr {
//    kind: IpAddrKind,
//    address: String,
//}

//fn coin_type(coin: Coin) -> u32 {
//    match coin {
//        Coin::Penny => {
//            println!("dumpster penny");
//            1
//        },
//        Coin::Nickel => 5,
//        Coin::Dime => 10,
//        Coin::Quarter(state) => {
//            println!("Quarter from {:?}", state);
//            25
//        },
//    }
//}

