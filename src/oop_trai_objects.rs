//use std::iter::Sum;
//#[derive(Debug)]
//pub struct AveragedCollection {
//    list: Vec<i32>,
//    average: f64,
//}
//
//impl AveragedCollection {
//    pub fn add(&mut self, x: i32) {
//        self.list.push(x);
//        self.update_average();
//    }
//
//    pub fn remove(&mut self) -> Option<i32> {
//        let result = self.list.pop();
//        match result {
//            Some(val) => {
//                self.update_average();
//                Some(val)
//            },
//            None => None,
//        }
//    }
//
//    pub fn average(&self) -> f64 {
//        self.average
//    }
//
//    pub fn update_average(&mut self) {
//        let sum: i32 = self.list.iter().sum();
//        self.average = sum as f64 / self.list.len() as f64;
//    }
//
//}

use branches::{Screen, Button, SelectBox};


fn main() {

//    let mut ac = AveragedCollection { list: vec![2, 3, 4], average: 1.0};
//    &ac.add(3);
//    &ac.add(5);
//    &ac.remove();
//
//    println!("{:?}", &ac);

    let screen = Screen {
        components: vec![
            Box::new(SelectBox {
                width: 75,
                height: 10,
                options: vec![
                    String::from("Yes"),
                    String::from("Maybe"),
                    String::from("No")
                ],
            }),
            Box::new(Button {
                width: 50,
                height: 10,
                label: String::from("OK"),
            }),
        ],

    };

    screen.run();
}