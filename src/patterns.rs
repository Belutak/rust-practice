


fn main() {
    let favorite_color: Option<&str> = None; //Some("green");
    let is_tuesday = false;
    let age: Result<u8, _> = "34".parse();

    if let Some(color) = favorite_color {
        println!("using your favorite color, {}, as the background", color);
    }
    else if is_tuesday {
        println!("Tuesday is green day!");
    } else if let Ok(age) = age {
        if age > 30 {
            println!("Using purple as the background color");
        } else {
            println!("Using orange as the background color");
        }
    } else {
        println!("using blue as the background color");
    }

    //while let
    let mut stack = Vec::new();

    stack.push(1);
    stack.push(2);
    stack.push(3);

    while let Some(top) = stack.pop() {
        println!("{}", top);
    }

    //for
    let v = vec!['a', 'b', 'c'];

    for (index, value) in v.iter().enumerate() {
        println!("{} is at index {}", value, index);
    }


    let points = (3, 5, 6);
    println!("{:?}", points);
    let point = (3, 5);
    print_coordinates(&point);


println!();


    let x = Some(5);
    let y = 10;

    match x {
        Some(50) => println!("its 50"),
        Some(y) => println!("matched, y = {:?}", y),
        _ => println!("default, x = {:?}", x),
    }
    println!("at end: x = {:?}, y = {:?}", x, y);

println!();

    let p = Point { x: 0, y: 7 };
    //let Point {x, y} = p;
    match p {
        Point { x, y: 0 } => println!("On the x axis at {}", x),
        Point { x: 0, y } => println!("On the y axis at {}", y),
        Point { x, y } => println!("On neither axis: ({}, {})", x, y),
    }

println!();
    //deconstructing enum
//    //let msg = Message::ChangeColor(0, 160, 255);
    let msg = Message::Write(String::from("GG"));
    match msg {
        Message::Quit => {
            println!("no data");
        },
        Message::Move {x, y} =>  {
            println!("Move in a x {} and y {} direction",
                x,
                y
            );
        },
        Message::Write(text) => println!("txt msg {}", text),
        Message::ChangeColor(r, g, b) => {
            println!(
                "Change the color to red {}, green {}, and blue {}",
                r,
                g,
                b
            )
        }
    }

    //deconstructing nested enum
//    let msg = Message::ChangeColor(Color::Hsv(0, 160, 255));
//
//    match msg {
//        Message::ChangeColor(Color::Rgb(r, g, b)) => {
//            println!("change color to r {}, g{}, b{}",
//            r,
//            g,
//            b
//            )
//        },
//        Message::ChangeColor(Color::Hsv(h, s, v)) => {
//            println!(
//                "change colors to h{}, s{}, v{}",
//                h,
//                s,
//                v
//            )
//        }
//        _ => ()
//    }


println!();

    //deconstructing references
    let points = vec![
        Point {x: 0, y: 0},
        Point {x: 1, y: 5},
        Point {x: 10, y: -3},
    ];

    let sum_of_squares: i32 = points
        .iter()
        .map(|&Point {x, y}| x * x + y * y)
        .sum();
    println!("sum is {}", sum_of_squares);

println!();

    let ((feet, inches), Point {x, y}) = ((3, 10), Point { x: 3, y: -10 });

println!();
    //ignoring val with _
    foo(3, 4);

    //ignoring parts of val with a nested _
    let mut setting_value = Some(5);
    let new_setting_value = Some(10);

    match (setting_value, new_setting_value) {
        (Some(_), Some(_)) => {
            println!("Can't overwrite an existing customized value");
        }
        _ => {
            setting_value = new_setting_value;
        }
    }

    println!("setting is {:?}", setting_value);


    let numbers = (2, 4, 8, 16, 32);

    match numbers {
        (first, _, third, _, fifth) => {
            println!("Some numbers: {}, {}, {}", first, third, fifth)
        },
    }

println!();

    //ignoring remaining parts of a val with ..
    let origin = Poin { x: 0, y: 0, z: 0 };

    match origin {
        Poin { x, .. } => println!("x is {}", x),
    }



    let numbers = (2, 4, 8, 16, 32);

    match numbers {
        (first, .., last) => {
            println!("Some numbers: {}, {}", first, last);
        },
    }

println!();

    //match guard
    let num = Some(4);

    match num {
        Some(x) if x < 5 => println!("less than five: {}", x),
        Some(x) => println!("{}", x),
        None => (),
    }




    let x = Some(5);
    let y = 10;

    match x {
        Some(50) => println!("Got 50"),
        Some(n) if n == y => println!("Matched, n = {:?}", n),
        _ => println!("Default case, x = {:?}", x),
    }

    println!("at the end: x = {:?}, y = {:?}", x, y);



    let x = 4;
    let y = false;

    match x {
        4 | 5 | 6 if y => println!("yes"),
        _ => println!("no"),
    }




println!();

    //@ Binding
    let nsg = Nessage::Hello { id: 5 };

    match nsg {
        Nessage::Hello { id: id_variable @ 3...7 } => {
            println!("found an id in range: {}", id_variable)
        },
        Nessage::Hello { id: 10...12 } => {
            println!("found 10-12")
        },
       Nessage::Hello { id } => {
           println!("Found other id: {}", id)
       },
    }

}


fn foo(_: i32, y: i32) {
    println!("This code only uses the y parameter: {}", y);
}

struct Point {
    x: i32,
    y: i32,
}

enum Nessage {
    Hello { id: i32 },
}

#[derive(Debug)]
enum Message {
    Quit,
    Move { x: i32, y: i32 },
    Write(String),
    ChangeColor(i32, i32, i32),
    //ChangeColor(Color),
}
#[derive(Debug)]
enum Color {
    Rgb(i32, i32, i32),
    Hsv(i32, i32, i32)
}

fn print_coordinates(&(x, y): &(i32, i32)) {
    println!("Current location: ({}, {})", x, y);
}

struct Poin {
    x: i32,
    y: i32,
    z: i32,
}