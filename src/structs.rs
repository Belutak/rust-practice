
#![allow(unused_variables)]

fn main() {

    struct User {
        username: String,
        email: String,
        sign_in_count: u64,
        active: bool,
    }

    let mut user1 = User {
        email: String::from("zoki@gmail.com"),
        username: String::from("ragnar"),
        active: true,
        sign_in_count,
    };

    user1.email = String::from("sexylover69@gmail.com");

    //struct update syntax
//    let user2 = User {
//        email: String::from("zorz@gmail.com"),
//        username: String::from("bjorn"),
//        active: user1.active,
//        sign_in_count: user1.sign_in_count,
//    };
    //..user1 means to take remaining fields values from user1
    let user2 = User {
        email: String::from("zorz@gmail.com"),
        username: String::from("bjorn"),
        ..user1
    };


    //tuple struct
    struct Color(i32, i32, i32);
    struct Point(i32, i32, i32);

    let black = Color(0, 0, 0);
    let origin = Point(0, 0 , 0);

    //unit like structs without fields  ()

}

fn build_user(emal: String, username: String) -> User {
    User {
        email,
        username,
        active: true,
        sign_in_count: 1,
    }
}

