use std::collections::HashMap;

fn main() {



//    let mut letters = HashMap::new();
//
// for ch in "a short treatise on fungi".chars() {
//     let counter = letters.entry(ch).or_insert(0);
//     *counter += 1;
// }
//
// assert_eq!(letters[&'s'], 2);
// assert_eq!(letters[&'t'], 3);
// assert_eq!(letters[&'u'], 1);
// assert_eq!(letters.get(&'y'), None);




    let mut list = vec![3, 2, 1, 9, 6, 5, 7, 2, 3, 4, 2, 6, 2, 4, 3];

    println!("{:?}", avg_val(&list));

    println!("{:?}", med_val(&list));

    println!("most repeated number is {:?}", mode_val(&list));
}

fn avg_val(vec: &Vec<i32>) -> f32 {
    let mut sum: f32 = 0.0;
    for i in  vec {
         sum += *i as f32;

    }

    return sum / vec.len() as f32;
}


fn med_val(vec: &Vec<i32>) -> f64 {
    let mut list_cpy =  &mut vec.clone();
    list_cpy.sort();

    let length = list_cpy.len();
    //let mut median = 0.0;
    if list_cpy.len() % 2 == 0 {
        return  (list_cpy[length / 2] + list_cpy[length / 2 + 1]) as f64 / 2.0;
    } else {
        return  list_cpy[length / 2] as f64;
    }
}

fn mode_val(vec: &Vec<i32>) -> i32 {
    let mut map = HashMap::new();

    for i in vec {
       let count = map.entry(i).or_insert(0);
        *count += 1;
    }
    //println!("mode_val fn  {:?}", map.get(&3));
    let mut max_val: i32 = 0;
    let mut key_val = 0;
    for (key, val) in map {
        if val >= max_val {
            max_val = val;
            key_val = *key;
        }
    }
   key_val
}