//use std::fs::File;

//enum Result<T, E> {
//    Ok(T),
//    Err(E),
//}
//
//enum Option<T> {
//    Some(T),
//    None,
//}


fn main() {

//    let z = 0;
//    exit_or_panic(Some(z));
//    exit_or_panic(None);
//    println!("x is {}", z);

//    let f = File::open("text.txt").unwrap();

//let mut x = 3;
//
//    let y = x + 1;
//
//    x = x + 2;
//    let z = x;
//    println!("x is {}", z);
//    println!("x is {}", y);
//    println!("X is {}", x);
//
//play(x);
//    println!("{}", play(x));
//
//
//    let guess = "42".parse::<u32>();
//    println!("guess is {:?}", guess);
//
//
//
//    let t = (1, "stringlet", 32.4, true);
//    let tt = t.1;
//
//
//    let mut a = [1, 2, 3, 4, 5, 6];
//
//    for element in a.iter() {
//        println!("element is {}", element);
//    }
//
//   println!("reversed {:?}", a.reverse());
//
//    for num in (1..5).rev() {
//        println!("{:?}", num);
//    }


//    let mut s1 = String::from("hello");
//
////    let (s2, len) = calculate_length(s1);
////
////    println!("The length of '{}' is {}.", s2, len);
//
////    let s2 = &mut s1;
//
//    let s3 = &mut s1;
//    println!("s3 is {}", s3);
//    s3.push_str(" under");
//    println!("and now s3 is {}", s3);
//    println!("s1 is {}", s1);
//
//    //println!("s2 is {}", s2);
//    println!("and now s3 is {}", s3);

        //i dont get it
//    let mut s1 = String::from("hello");
//    println!("s1 is {}", s1);
//
//    let s2 = &mut s1;
//    println!("s2 is {}", s2);
//
//    s2.push_str(" under");
//    println!("s2 is now {}", s2);
//
//    println!("s1 is now {}", &mut s1);
//
//    println!("s2 newest is {}", s2);


    let mut s = String::from("hello underworld");

    let b = &s[0..3];
    println!("slice is {}", b);

        println!("is {}", slices_test(&mut s));

    println!("s is {}", s);
    let bg = &s[0..3];
    println!("slice is {}", bg);
//    let m =  s;
//println!("m is {}", m);

    println!();
    println!("sliced txt is {}", slice_dice(&s));





    let mut alpha_being = Person {
        name: String::from("HighGround"),
        age: 32,
        alive: true,
    };


    let g = alpha_being.name;
    println!("name of being is {}", g);

    alpha_being.name = String::from("LowGround");

    println!("position that does not have advantage {}", alpha_being.name);

    //println!("peson is {:?}", alpha_being);
    dbg!(&alpha_being);



    let newborn = create_being(String::from("Jupiter"), 22);
    dbg!(newborn);

    let firstborn = Person {
        name: String::from("Mars"),
        ..alpha_being
    };
    dbg!(firstborn);


    let c = Circle::new_circle(20.2);
    dbg!(c);
}



fn slices_test(s: &String) -> usize {
    let bytes = s.as_bytes();

    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return i;
        }
    }

    s.len()




}


//fn exit_or_panic(x: Option<i32>) {
//    match x {
//        Some(0) => panic!("x is zero"),
//        Some(x) => println!("x is above zero"),
//        None => println!("nothing"),
//    }
//
//}

//fn play(num: i32) -> i32 {
//    println!("x in fn is {}", num);
//    num + 1
//}

//fn calculate_length(s: String) -> (String, usize) {
//    let length = s.len(); // len() returns the length of a String
//
//    (s, length)
//}

fn slice_dice(s: &String) -> &str {
    let byte = s.as_bytes();

    for (i, &item) in byte.iter().enumerate() {
        if item == b' ' {
            return &s[0..i];
        }
    }
    &s[..]
}

#[derive(Debug)]
struct Person {
    name: String,
    age: i32,
    alive: bool,
}
#[derive(Debug)]
struct Circle {
    rad: f32,
    pi: f32,
}

impl Circle {
    fn circle_calc(&self) -> f32 {
        self.rad * 2.0 * self.pi
    }

    fn can_contain(&self, other: &Circle) -> bool {
        self.rad > other.rad
    }

    fn new_circle(rad: f32) -> Circle {
        Circle {rad, pi: 3.14}
    }
}

fn create_being(name: String, age: i32) -> Person {
  Person {
      name,
      age,
      alive: true,
  }
}