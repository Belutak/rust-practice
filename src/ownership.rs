


fn main() {
//    let mut s = String::from("hello");
//
//    s.push_str(", world"); //push_str appends a literal to a String
//
//    println!("{}", s);


//    //after moving pointer from s to s2, s cannot be use anymore, this gives error
//    let s = String::from("hello");
//    let s2 = s;
//
//    println!("{}, world", s);


//    //make deep copy(copy heap data and not just make new pointer to same heap data)
//    let s1 = String::from("hello");
//    let s2 = s1.clone();
//
//    println!("s1 = {}, s2 = {}", s1, s2);


//    //ownership and functions
//    let s = String::from("hello");
//
//    //s moves into functions and its no longer valid here
//    takes_ownership(s);
//
//    let x = 5;
//    //i32 is Copy so it remains valid after being used in function
//    makes_copy(x);


//    //return values and scope
//    let s1 = gives_ownership();
//
//    let s2 = String::from("hello");
//
//    let s3 = takes_and_gives_back(s2);


    //return multiple values
    let s1 = String::from("hello");

    let (s2, len) = calculate_length(s1);

    println!("The length of '{}' is {}.", s2, len);
}

//fn takes_ownership(some_string: String) { //some string comes into scope
//    println!("{}", some_string);
//}                                       //drop is called and some_string is dropped from memory
//
//fn makes_copy(some_integer: i32) {
//    println!("{}", some_integer);
//}


//fn gives_ownership() -> String {
//
//    let some_string = String::from("hello");
//
//    some_string
//}
//
//fn takes_and_gives_back(a_string: String) -> String {
//
//    a_string
//}


fn calculate_length(s: String) -> (String, usize) {
    let length = s.len();
    (s, length)
}
