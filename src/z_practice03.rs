//module named sound
mod sound {
    pub mod instrument {
        pub fn clarinet() {
            super::breathe();
        }
    }

    fn breathe() {}
}








//module truth
mod truth{
#[derive(Debug)]
    pub struct Truth {
        pub name: String,
        id: i32,
    }

    impl Truth {
        pub fn new_truth(name: &str) -> Truth {
            Truth {
                name: String::from(name),
                id: 1,
            }
        }
    }

}




fn main() {
    crate::sound::instrument::clarinet();

    sound::instrument::clarinet();

    let mut t = crate::truth::Truth::new_truth("i might fail and its ok");
    println!("the truth is {:?}", t);
    println!("just the truth {} - ", t.name);

    t.name = String::from("earth is a playground");
    println!("the truth is {}", t.name);


}
