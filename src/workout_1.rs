const HEX: &str = "Man";

fn to_base64(hex_input: &str) -> String {
    let mut ascii_chars: Vec<u8> = vec![];

    for c in hex_input.chars() {
        ascii_chars.push(c as u8);
    }

    println!("{:?}", ascii_chars);

    let mut bin_repr = String::from("");

    for ac in ascii_chars {
        bin_repr.push_str(&format!("{:08b}", ac));
    }

    println!("{:?}", bin_repr);

    let mut sextet: Vec<char> = vec![];

    // The Base64 index table alphabet starts with A at index 0, B at index 1, etc.
    let b64_offset_uppercase = 65;
    let b64_offset_lowercase = 71;
    let b64_offset_digit: i32 = -4;
    let mut base64_output = String::from("");

    for bit in bin_repr.chars() {
        sextet.push(bit);

        if sextet.len() == 6 {
            let bin_str: String = sextet.iter().collect();
            println!("{}", bin_str);

            base64_output.push_str(&format!(
                "{}",
                u32::from_str_radix(&bin_str, 2)
                    .ok()
                    .and_then(|n| {
                        if n >= 0 && n <= 25 {
                            ::std::char::from_u32(n + b64_offset_uppercase)
                        } else if n >= 26 && n <= 51 {
                            ::std::char::from_u32(n + b64_offset_lowercase)
                        } else if n >= 52 && n <= 61 {
                            ::std::char::from_u32((n + b64_offset_digit as u32))
                        } else {
                            None
                        }
                    })
                    .unwrap()
            ));

            sextet.clear();
        }
    }
    base64_output
}

pub fn run() {
    println!("{}", to_base64(HEX));
}

fn main() {
    run();
}