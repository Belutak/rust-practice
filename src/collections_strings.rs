


fn main() {

//    let mut s0 = String::new();
//
//    let data = "initial data";
//    let s = data.to_string();
//
//    let s1 = "inital data".to_string();
//
//    let s2 = String::from("string data");

    let mut s3 = String::from("foo");
    //s3.push_str("bar");
    let s2 = "bar";
    s3.push_str(s2);
    println!("s2 is {}", s2);


    let mut s1 = String::from("lo");
    s1.push('l');
    println!("{}", s1);

    let ss1 = String::from("Hello, ");
    let ss2 = String::from("world!");
    let ss3 = ss1 + &ss2;
    println!("ss3 is {}", ss3);

//    let t1 = String::from("tic");
//    let t2 = String::from("tac");
//    let t3 = String::from("toe");
//    let t4 = t1 + " " + &t2 + " " + &t3;
//    println!("{}", t4);

    let t1 = String::from("tic");
    let t2 = String::from("tac");
    let t3 = String::from("toe");
    let t4 = format!("{}-{}-{}", t1,t2,t3);
    println!("format! {}", t4);

//    let len = String::from("Здравствуйте").len();
//    !dbg!(len);

    //cannot intex into value of type usize
    //let len2 = &len[0];

    let hello = String::from("Здравствуйте");
    //let hello = "Здравствуйте";
    let len2 = &hello[0..4];
    println!("len2 is {}", len2);

    //separate and print chars
    for c in hello.chars() {
        println!("{}", c);
    }

    //separates and return bytes
    for b in hello.bytes() {
        println!("{}", b);
    }
}