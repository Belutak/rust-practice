use std::fmt::Result;
use std::io::Result as IoResult;




mod sound {
    pub mod instrument {

            pub fn clarinet() {
                //fn
            }


    }
}

//use self::sound::instrument;

//mod performance_group {
//    use crate::sound::instrument;
//
//    pub fn clarinet_trio() {
//        instrument::clarinet();
//        instrument::clarinet();
//        instrument::clarinet();
//    }
//}


use self::sound::instrument::clarinet;


//mod sound {
//    mod instrument {
//        fn clarinet() {
//            super::breathe_in();
//        }
//    }
//
//    fn breathe_in() {
//        //function code
//    }
//}




mod plant {
    pub struct Vegetable {
        pub name: String,
        id: i32,
    }

    impl Vegetable {
        pub fn new(name: &str) -> Vegetable {
            Vegetable {
                name: String::from(name),
                id: 1,
            }
        }
    }
}




mod menu {
    pub enum Appetizer {
        Soup,
        Salad,
    }
}






mod performance_group {
    pub use crate::sound::instrument;

    pub fn clarinet_trio() {
        instrument::clarinet();
        instrument::clarinet();
        instrument::clarinet();
    }
}

fn main() {

    //    //absolute path
    //    crate::sound::instrument::clarinet();
    //
    //    //relative path
    //    sound::instrument::clarinet();

//    let mut v = plant::Vegetable::new("squash");
//
//    v.name = String::from("butternut squash");
    println!("{} are delicious", v.name);
        //wont compile because id is private
    //println!("The ID is {}", v.id);


//    let order1 = menu::Appetizer::Soup;
//    let order2 = menu::Appetizer::Salad;



    //instrument::clarinet();



//    performance_group::clarinet_trio();


    clarinet();



    performance_group::clarinet_trio();
    performance_group::instrument::clarinet();
}