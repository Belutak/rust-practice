

enum SpreadsheetCell {
    Int(i32),
    Float(f64),
    Text(String),
}




fn main() {

    //let v: Vec<i32> = Vec::new();

    //let vector = vec![1, 2, 3];

//    let mut v = Vec::new();
//
//    v.push(1);
//    v.push(5);
//    v.push(6);




//    let v = vec![1, 2, 3, 4, 5];

//    let third: &i32 = &v[2];
//    println!("third element is {}", third);
//
//    match v.get(2) {
//        Some(third) => println!("the third element is {}", third),
//        None => println!("There is no third element"),
//    }


    //breaking it
//    let _noexist = &v[100];
//    let _existno = v.get(100);
//    println!("v no {:?}", v.get(123));




    //cant have mutable in immutable reference in same scope
//    let mut v = vec![1, 2, 3, 4, 5];
//
//    let first = &v[0];
//
//    v.push(6);
//
//    println!("The first element is: {}", first);


    //iterating through immutable values in vector
//    let v = vec![100, 200, 300];
//    for i in &v {
//        println!("{}", i);
//    }

    //iterating and changing mut values in vector
//    let mut v = vec![12, 1, 10];
//    for i in &mut v {
//        *i += 50;
//        println!("{}", i);
//    }


    //storing enums in vector
    let row = vec![
        SpreadsheetCell::Int(3),
        SpreadsheetCell::Text(String::from("blue")),
        SpreadsheetCell::Float(10.12),
    ];
}