//enum List {
//    Cons(i32, Box<List>),
//    Nil,
//}
//
//use List::{Cons, Nil};

//use std::ops::Deref;
//
//struct MyBox<T>(T);
//
//impl<T> MyBox<T> {
//    fn new(x: T) -> MyBox<T> {
//        MyBox(x)
//    }
//}
//
//impl<T> Deref for MyBox<T> {
//    type Target = T;
//
//    fn deref(&self) -> &T {
//        &self.0
//    }
//}

//struct CustomSmartPointer {
//    data: String,
//}
//
//impl Drop for CustomSmartPointer {
//    fn drop(&mut self) {
//        println!("Dropping CustomSmartPointer with val '{}'", self.data);
//    }
//}

enum List {
    Cons(i32, Rc<List>),
    Nil,
}

use std::rc::Rc;
use List::{Cons, Nil};

fn main() {
//    let a = Rc::new(Cons(5, Rc::new(Cons(10, Rc::new(Nil)))));
//    let b = Cons(3, Rc::clone(&a));
//    let c = Cons(4, Rc::clone(&a));

    let a = Rc::new(Cons(5, Rc::new(Cons(10, Rc::new(Nil)))));
    println!("count after creating a = {}", Rc::strong_count(&a));
    let b = Cons(3, Rc::clone(&a));
    println!("count after creating b = {}", Rc::strong_count(&a));
    {
        let c = Cons(4, Rc::clone(&a));
        println!("count after creating c = {}", Rc::strong_count(&a));
    }
    println!("count after c goes out of scope = {}", Rc::strong_count(&a));

    //    let c = CustomSmartPointer { data: String::from("my stuff") };
    //    println!("CustomSmartPointers created.");
    //    drop(c);
    //    let d = CustomSmartPointer { data: String::from("other stuff") };

    //let b = Box::new(5);
    //    println!("b = {}", b);

    //    let list = Cons(1,
    //                    Box::new( Cons(2,
    //                                   Box::new(Cons( 3,
    //                                                  Box::new(Nil))))));

    //    let x = 5;
    //    let y = MyBox::new(x);
    //
    //    assert_eq!(5, x);
    //    assert_eq!(5, *y);

    //    let m = MyBox::new(String::from("Rust"));
    //    hello(&m);
}

//fn hello(name: &str) {
//    println!("Hey, {}", name);
//}
