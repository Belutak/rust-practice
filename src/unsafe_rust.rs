
use std::slice;

static HELLO_WORLD: &str = "Hello world";
static mut COUNTER: u32 = 0;

fn add_to_count(inc: u32) {
    unsafe {
        COUNTER += inc;
    }
}


//    let len = slice.len();
fn main() {
    //craete immutable and mutable raw pointers
    let mut num = 5;

    let r1 = &num as *const i32;
    let r2 = &mut num as *mut i32;
    //uncertain address
    let address = 0x012345usize;
    let r = address as *const i32;
    //we can create raw pointer but cant deref them out of unsafe
    unsafe {
        println!("r1 is: {}", *r1);
        println!("r2 is: {}", *r2);
    }

    //calling unsafe fn or method
    unsafe fn dangerous() {}

    unsafe {
        dangerous();
    }

    //safe abstraction over unsafe code
    let mut v = vec![1, 2, 3, 4, 5, 6];

    let r = &mut v[..];

    let (a, b) = r.split_at_mut(3);


    //bad usage of unsafe
//    let address = 0x01234;
//    let r = address as *mut i32;
//
//    let slice : &[i32] = unsafe {
//        slice::from_raw_parts_mut(r, 10000)
//    };

    unsafe {
        println!("Abs val of -3 according to C is: {}", abs(-3));
    }


    //static variables
    println!("name is: {}", HELLO_WORLD);



}

//impl unsafe trait

unsafe trait Foo {
    // methods go here
}

unsafe impl Foo for i32 {
    // method implementations go here
}


//fn split_at_mut(slice: &mut [i32], mid: usize) -> (&mut [i32], &mut [i32]) {
//
//    assert!(mid <= len);
//
//    (&mut slice[..mid],
//    &mut slice[mid..])
//}

fn split_at_mut(slice: &mut [i32], mid: usize) -> (&mut [i32], &mut [i32]) {
    let len = slice.len();
    let ptr = slice.as_mut_ptr();

    assert!(mid <= len);

    unsafe {
        (slice::from_raw_parts_mut(ptr, mid),
        slice::from_raw_parts_mut(ptr.offset(mid as isize), len - mid))
    }



}

extern "C" {
    fn abs(input: i32) -> i32;
}