use std::collections::HashMap;

fn main() {

    let mut scores = HashMap::new();

    scores.insert(String::from("Blue"), 10);
    //scores.insert(String::from("Yellow"), 50);

//    let teams =
//        vec![String::from("Blue"), String::from("Yellow")];
//    let initial_scores = vec![10, 50];
//
//    let scores: HashMap<_, _> = teams.iter()
//        .zip(initial_scores.iter()).collect();




//    let field_name = String::from("Bestest color");
//    let field_val = String::from("Green");
//    //let field_val = 4;
//
//    let mut map = HashMap::new();
//    map.insert(field_name, field_val);
//    //println!("name and val {}, {}", field_name, field_val);




//    let team_name = String::from("Blue");
//    let score = scores.get(&team_name);
//    //dbg!(score);




//    for (key, value) in &scores {
//        println!("{}: {}", key, value);
//    }




    //updating a hash map

    //overwriting a value
//    scores.insert(String::from("Blue"), 25);
//    println!("{:?}", scores);

    //insert if a key has no value
    scores.entry(String::from("Yellow")).or_insert(50);
    scores.entry(String::from("Blue")).or_insert(50);
    println!("{:?}", scores);

    //updating a value based on the old value
    let text = "hello world wonderful world";

    let mut map = HashMap::new();

    for word in text.split_whitespace() {
        let count = map.entry(word).or_insert(0);
        *count += 1;
    }
    println!("{:?}", map);
}