impl Rectangle {
    fn area(&self) -> u32 {
        self.width * self.height
    }
}  //diff between &self and self