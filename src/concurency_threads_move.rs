

use std::thread;
use std::time::Duration;




fn main() {
    let v = vec![1, 2, 3];

    let handle = thread::spawn(move || {
        println!("Vector vals are: {:?}", v);
    });

    //drop(v);

    handle.join().unwrap();

//    let handle = thread::spawn(|| {
//        for i in 1..10 {
//            println!("hello no {} from spawned thread!", i);
//            thread::sleep(Duration::from_millis(1));
//        }
//    });
//
//    handle.join().unwrap();

//    for i in 1..5 {
//        println!("heyo number {} from the main thread!", i);
//        thread::sleep(Duration::from_millis(1));
//    }

}