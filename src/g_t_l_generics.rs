#![allow(unused_variables)]

struct Point<T, U> {
    x: T,
    y: U,
}

impl<T, U> Point<T, U> {
    fn mixup<V, W>(self, other: Point<V, W>) -> Point<T, W> {
        Point {
            x: self.x,
            y: other.y,
        }
    }
}

//impl<T> Point<T> {
//    fn x(&self) -> &T {
//        &self.x
//    }
//}
//
//impl Point<f32> {
//    fn coords(&self) -> f32 {
//        (self.x.powi(2) + self.y.powi(2)).sqrt()
//    }
//}

//fn largest<T>(list: &[T]) -> T {
//    let mut largest = list[0];
//
//    for &item in list.iter() {
//        if item > largest {
//            largest = item;
//        }
//    }
//
//    largest
//}

//fn largest_char(list: &[T]) -> char {
//    let mut largest = list[0];
//
//    for &element in list.iter() {
//        if element > largest {
//            largest = element;
//        }
//    }
//
//    largest
//}

fn main() {
//    let number_list = vec![34, 50, 25, 100, 65];
//
//    let result = largest(&number_list);
//    println!("The largest number is {}", result);
//
//    let char_list = vec!['d', 'g', 'm', 'o'];
//
//    let result = largest(&char_list);
//    println!("The largest number is {}", result);



//    let integer = Point { x: 5, y: 10 };
//    let float = Point { x: 1.0, y: 4.0 };
//    //    let integer_and_float = Point { x: 5, y: 4.0 };
//    println!("p.x = {}", integer.x());


    let p1 = Point { x: 5, y: 10.4 };
    let p2 = Point { x: "Hello", y: 'c'};

    let p3 = p1.mixup(p2);

    println!("p3.x = {}, p3.y = {}", p3.x, p3.y);
}





//fn largest<T: PartialOrd + Clone>(list: &[T]) -> T {
//    let mut clone_vec = list.clone();
//    let mut largest = clone_vec[0];
//
//
//    for &item in clone_vec.iter(){
//        if item > largest {
//            largest = item;
//        }
//    }
//
//    largest
//}
//
//
//fn main() {
//    let number_list = vec![34, 50, 25, 100, 65];
//
//    let result = largest(&number_list);
//    println!("The largest number is {}", result);
//
//    let char_list = vec!['d', 'g', 'm', 'o'];
//
//    let result = largest(&char_list);
//    println!("The largest char is {}", result);
//
//}